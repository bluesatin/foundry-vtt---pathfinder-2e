import { ActorDataPF2e } from '@actor/data-definitions';
import { ItemDataPF2e } from '@item/data/types';
import { MigrationBase } from './migrations/base';

interface ItemsDiff {
    inserted: ItemDataPF2e[];
    deleted: string[];
    updated: ItemDataPF2e[];
}

export class MigrationRunnerBase {
    latestVersion: number;
    migrations: MigrationBase[];

    constructor(migrations: MigrationBase[] = []) {
        this.migrations = migrations.sort((a, b) => a.version - b.version);
        this.latestVersion = 'game' in globalThis ? globalThis.game.system.data.schema : 0;
    }

    needsMigration(currentVersion: number): boolean {
        return currentVersion < this.latestVersion;
    }

    diffItems(orig: ItemDataPF2e[], updated: ItemDataPF2e[]): ItemsDiff {
        const ret: ItemsDiff = {
            inserted: [],
            deleted: [],
            updated: [],
        };

        const origItems: Map<string, ItemDataPF2e> = new Map();
        for (const item of orig) {
            origItems.set(item._id, item);
        }

        for (const item of updated) {
            const origItem = origItems.get(item._id);
            if (origItem) {
                // check to see if anything changed
                if (JSON.stringify(origItem) !== JSON.stringify(item)) {
                    ret.updated.push(item);
                }
                origItems.delete(item._id);
            } else {
                // it's new
                ret.inserted.push(item);
            }
        }

        // since we've been deleting them as we process, the ones remaining need to be deleted
        for (const item of origItems.values()) {
            ret.deleted.push(item._id);
        }

        return ret;
    }

    async getUpdatedItem(item: ItemDataPF2e, migrations: MigrationBase[]): Promise<ItemDataPF2e> {
        const current = duplicate(item);

        for (const migration of migrations) {
            try {
                await migration.updateItem(current);
            } catch (err) {
                console.error(err);
            }
        }

        return current;
    }

    async getUpdatedActor(actor: ActorDataPF2e, migrations: MigrationBase[]): Promise<ActorDataPF2e> {
        const current = duplicate(actor);

        for (const migration of migrations) {
            try {
                await migration.updateActor(current);
                for (const item of current.items) {
                    await migration.updateItem(item, current);
                }
            } catch (err) {
                console.error(err);
            }
        }

        return current;
    }

    async getUpdatedMessage(messageData: ChatMessageData, migrations: MigrationBase[]): Promise<ChatMessageData> {
        const current = duplicate(messageData);

        for (const migration of migrations) {
            try {
                await migration.updateMessage(current);
            } catch (err) {
                console.error(err);
            }
        }

        return current;
    }

    async getUpdatedMacro(macroData: MacroData, migrations: MigrationBase[]): Promise<MacroData> {
        const current = duplicate(macroData);

        for (const migration of migrations) {
            try {
                await migration.updateMacro(current);
            } catch (err) {
                console.error(err);
            }
        }

        return current;
    }

    async getUpdatedTable(table: RollTableData, migrations: MigrationBase[]): Promise<RollTableData> {
        const current = duplicate(table);

        for (const migration of migrations) {
            try {
                await migration.updateTable(current);
            } catch (err) {
                console.error(err);
            }
        }

        return current;
    }

    async getUpdatedToken(tokenData: TokenData, migrations: MigrationBase[]): Promise<TokenData> {
        const current = duplicate(tokenData);
        for (const migration of migrations) {
            try {
                await migration.updateToken(current);
            } catch (err) {
                console.error(err);
            }
        }

        return current;
    }

    async getUpdatedUser(userData: UserData, migrations: MigrationBase[]): Promise<UserData> {
        const current = duplicate(userData);
        for (const migration of migrations) {
            try {
                await migration.updateUser(current);
            } catch (err) {
                console.error(err);
            }
        }

        return current;
    }
}
